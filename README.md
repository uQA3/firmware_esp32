uQA3 ESP32 firmware
===================
# Running in Docker

Make sure you have docker installed. 

- [Docker for Linux](https://docs.docker.com/install/linux/docker-ce/debian/)

Once installed, go to the firmware_esp32 folder and enter the dockershell.

```
    cd firmware_esp32
    ./dockershell
```

You need to run Docker with sudo, unless you add your user to docker group.

```
    usermod -aG docker $USERNAME
```

You're ready!

# Running Stand Alone

## Dependencies

### Prerequisites
```
    sudo apt-get install gcc git wget make libncurses-dev flex bison gperf python python-serial
```

### Toolchain

 - 64 bit for Linux: https://dl.espressif.com/dl/xtensa-esp32-elf-linux64-1.22.0-80-g6c4433a-5.2.0.tar.gz
 - 32 bit for Linux: https://dl.espressif.com/dl/xtensa-esp32-elf-linux32-1.22.0-80-g6c4433a-5.2.0.tar.gz

```
    mkdir -p ~/esp
    cd ~/esp
    tar -xzf ~/Downloads/xtensa-esp32-elf-linux64-1.22.0-80-g6c4433a-5.2.0.tar.gz
```
To make xtensa-esp32-elf available for all terminal sessions, add the following line to your ~/.profile file:
```
    export PATH="$PATH:$HOME/esp/xtensa-esp32-elf/bin"
```

### ESP-IDF
```
    cd ~/esp
    git clone --recursive https://github.com/espressif/esp-idf.git
```
Set up IDF\_PATH by adding the following line to ~/.profile file:
```
    export IDF_PATH=~/esp/esp-idf
```

#### Install the required python packages
```
    sudo python -m pip install -r $IDF_PATH/requirements.txt
```

#### Install the extra-required ESP components
```
    git clone https://github.com/kylehendricks/esp32-vl53l0x $IDF-PATH/components/esp32-vl53l0x
```

More detailed information about ESP32 available [here](https://docs.espressif.com/projects/esp-idf/en/latest/index.html)


# Getting started!

```
    mkdir build && cd build
    cmake ..
    make menuconfig
    make
```

## Build and flash

```
    make flash
```

## Output monitoring

```
    make monitor
```



# Troubleshooting
If you got an error like
```
esp32 fatal error: export.h: No such file or directory
```
do the next:
```
rm $IDF_PATH/components/libsodium/port/randombytes_esp32.c
rm $IDF_PATH/components/libsodium/port/crypto_hash_mbedtls/crypto_hash_sha256_mbedtls.c
rm $IDF_PATH/components/libsodium/port/crypto_hash_mbedtls/crypto_hash_sha512_mbedtls.c
```

