cmake_minimum_required(VERSION 3.5)

set(MAIN_SRCS
    main/main.c
    main/libs/mpu6050.c
    main/libs/filter.c
    main/libs/kalman.c
    main/libs/pwm.c
    main/libs/udp_multicast.c
    main/libs/vl53l0x_helper.c
    main/libs/bme280.c
    main/libs/leds.c
    main/libs/wifi.c
    main/libs/coap-handler.c
    main/libs/timer.c
    main/libs/PID.c

    main/services/service.c

    main/services/motors/service.c
    main/services/light_code/service.c
    main/services/imu/service.c
    main/services/mitm/service.c
    main/services/baro/service.c
    main/services/tof/service.c
    main/services/coap/service.c
    main/services/coap/endpoints.c
    main/services/sys/service.c
    )

include($ENV{IDF_PATH}/tools/cmake/project.cmake)

include_directories("build/include")
include_directories("main")

project(uQA3 C)
