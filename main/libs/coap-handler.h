#ifndef COAP_CLIENT_LIB_H
#define COAP_CLIENT_LIB_H

#include "coap.h"
#include <cJSON.h>

void coap_init(void);

coap_context_t * coap_get_free_context();
void coap_release_context(coap_context_t * ctx);

void create_dst_addr(coap_address_t * dst_addr, const char * server_uri);
void send_json_message( const cJSON * fld , const char * target_uri);
uint16_t coap_next_message_id(void);
#endif
