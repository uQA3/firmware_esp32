#include "pwm.h"

#include "driver/mcpwm.h"
#include "soc/mcpwm_reg.h"
#include "soc/mcpwm_struct.h"

#include "globals.h"


void pwm_init(void)
{
	mcpwm_gpio_init(MCPWM_UNIT_0, MCPWM0A, GPIO_PWM0A_OUT);
	mcpwm_gpio_init(MCPWM_UNIT_0, MCPWM1A, GPIO_PWM1A_OUT);
	mcpwm_gpio_init(MCPWM_UNIT_1, MCPWM0A, GPIO_PWM0B_OUT);
	mcpwm_gpio_init(MCPWM_UNIT_1, MCPWM1A, GPIO_PWM1B_OUT);

	mcpwm_config_t pwm_config;
	pwm_config.frequency = 1000;
	pwm_config.cmpr_a = 0;
	pwm_config.cmpr_b = 0;
	pwm_config.counter_mode = MCPWM_UP_COUNTER;
	pwm_config.duty_mode = MCPWM_DUTY_MODE_0;

	mcpwm_init(MCPWM_UNIT_0, MCPWM_TIMER_0, &pwm_config);
	mcpwm_init(MCPWM_UNIT_0, MCPWM_TIMER_1, &pwm_config);
	mcpwm_init(MCPWM_UNIT_1, MCPWM_TIMER_0, &pwm_config);
	mcpwm_init(MCPWM_UNIT_1, MCPWM_TIMER_1, &pwm_config);
}

void pwm_set_duty(int pwm, float duty_cycle)
{
	int mcpwm_num, timer_num;

	switch (pwm) {
	case 1:
		mcpwm_num = MCPWM_UNIT_0;
		timer_num = MCPWM_TIMER_1;
		break;
	case 2:
		mcpwm_num = MCPWM_UNIT_1;
		timer_num = MCPWM_TIMER_1;
		break;
	case 3:
		mcpwm_num = MCPWM_UNIT_0;
		timer_num = MCPWM_TIMER_0;
		break;
	case 4:
		mcpwm_num = MCPWM_UNIT_1;
		timer_num = MCPWM_TIMER_0;
		break;
	default:
		return;
	}

	mcpwm_set_signal_low(mcpwm_num, timer_num, MCPWM_OPR_B);
	mcpwm_set_duty(mcpwm_num, timer_num, MCPWM_OPR_A, duty_cycle);
	mcpwm_set_duty_type(mcpwm_num, timer_num, MCPWM_OPR_A, MCPWM_DUTY_MODE_0);
}
