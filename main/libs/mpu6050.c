#include "mpu6050.h"

#include <math.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <driver/i2c.h>
#include <esp_log.h>
#include "driver/gpio.h"
#include "sdkconfig.h"

#undef ESP_ERROR_CHECK
//#define ESP_ERROR_CHECK(x)   do { esp_err_t rc = (x); if (rc != ESP_OK) { ESP_LOGE("err", "esp_err_t = %d", rc); assert(0 && #x);} } while(0);
#define ESP_ERROR_CHECK(x)   do { esp_err_t rc = (x); if (rc != ESP_OK) { ESP_LOGE("err", "esp_err_t = %d", rc); } } while(0);

void mpu6050_init(void)
{
	i2c_cmd_handle_t cmd;

	cmd = i2c_cmd_link_create();
	ESP_ERROR_CHECK(i2c_master_start(cmd));
	ESP_ERROR_CHECK(i2c_master_write_byte(cmd, (I2C_ADDRESS << 1) | I2C_MASTER_WRITE, 1));
	i2c_master_write_byte(cmd, MPU6050_ACCEL_XOUT_H, 1);
	ESP_ERROR_CHECK(i2c_master_stop(cmd));
	i2c_master_cmd_begin(I2C_NUM_0, cmd, NULL);
	i2c_cmd_link_delete(cmd);

	cmd = i2c_cmd_link_create();
	ESP_ERROR_CHECK(i2c_master_start(cmd));
	ESP_ERROR_CHECK(i2c_master_write_byte(cmd, (I2C_ADDRESS << 1) | I2C_MASTER_WRITE, 1));
	i2c_master_write_byte(cmd, MPU6050_PWR_MGMT_1, 1);
	i2c_master_write_byte(cmd, 0, 1);
	ESP_ERROR_CHECK(i2c_master_stop(cmd));
	i2c_master_cmd_begin(I2C_NUM_0, cmd, NULL);
	i2c_cmd_link_delete(cmd);

	/* config accel to 4g  */
	cmd = i2c_cmd_link_create();
	ESP_ERROR_CHECK(i2c_master_start(cmd));
	ESP_ERROR_CHECK(i2c_master_write_byte(cmd, (I2C_ADDRESS << 1) | I2C_MASTER_WRITE, 1));
	i2c_master_write_byte(cmd, ACCEL_CONFIG , 1);
	i2c_master_write_byte(cmd, ACCEL_FS_SEL_4G, 1);
	ESP_ERROR_CHECK(i2c_master_stop(cmd));
	i2c_master_cmd_begin(I2C_NUM_0, cmd, NULL);
	i2c_cmd_link_delete(cmd);

	/* config gyro to 2000dps  */
	cmd = i2c_cmd_link_create();
	ESP_ERROR_CHECK(i2c_master_start(cmd));
	ESP_ERROR_CHECK(i2c_master_write_byte(cmd, (I2C_ADDRESS << 1) | I2C_MASTER_WRITE, 1));
	i2c_master_write_byte(cmd, GYRO_CONFIG , 1);
	i2c_master_write_byte(cmd, GYRO_FS_SEL_2000DPS, 1);
	ESP_ERROR_CHECK(i2c_master_stop(cmd));
	i2c_master_cmd_begin(I2C_NUM_0, cmd, NULL);
	i2c_cmd_link_delete(cmd);

	/* set accel bandwidth */
	cmd = i2c_cmd_link_create();
	ESP_ERROR_CHECK(i2c_master_start(cmd));
	ESP_ERROR_CHECK(i2c_master_write_byte(cmd, (I2C_ADDRESS << 1) | I2C_MASTER_WRITE, 1));
	i2c_master_write_byte(cmd, ACCEL_CONFIG2 , 1); 
	i2c_master_write_byte(cmd, ACCEL_DLPF_5, 1); 
	ESP_ERROR_CHECK(i2c_master_stop(cmd));
	i2c_master_cmd_begin(I2C_NUM_0, cmd, NULL);
	i2c_cmd_link_delete(cmd);

	/* set gyro bandwidth */
	cmd = i2c_cmd_link_create();
	ESP_ERROR_CHECK(i2c_master_start(cmd));
	ESP_ERROR_CHECK(i2c_master_write_byte(cmd, (I2C_ADDRESS << 1) | I2C_MASTER_WRITE, 1));
	i2c_master_write_byte(cmd, CONFIG , 1); 
	i2c_master_write_byte(cmd, GYRO_DLPF_5, 1); 
	ESP_ERROR_CHECK(i2c_master_stop(cmd));
	i2c_master_cmd_begin(I2C_NUM_0, cmd, NULL);
	i2c_cmd_link_delete(cmd);

}

void mpu6050_fetchData(imuData_t *data_imu)
{
	i2c_cmd_handle_t cmd;

	uint8_t data[24];

	short accel_x = 0;
	short accel_y = 0;
	short accel_z = 0;
	short gyro_x = 0;
	short gyro_y = 0;
	short gyro_z = 0;
	short mag_x = 0;
	short mag_y = 0;
	short mag_z = 0;

	cmd = i2c_cmd_link_create();
	ESP_ERROR_CHECK(i2c_master_start(cmd));
	ESP_ERROR_CHECK(i2c_master_write_byte(cmd, (I2C_ADDRESS << 1) | I2C_MASTER_WRITE, 1));
	ESP_ERROR_CHECK(i2c_master_write_byte(cmd, MPU6050_ACCEL_XOUT_H, 1));
	ESP_ERROR_CHECK(i2c_master_stop(cmd));
	ESP_ERROR_CHECK(i2c_master_cmd_begin(I2C_NUM_0, cmd, NULL));
	i2c_cmd_link_delete(cmd);

	cmd = i2c_cmd_link_create();
	ESP_ERROR_CHECK(i2c_master_start(cmd));
	ESP_ERROR_CHECK(i2c_master_write_byte(cmd, (I2C_ADDRESS << 1) | I2C_MASTER_READ, 1));

	ESP_ERROR_CHECK(i2c_master_read_byte(cmd, data,   0));
	ESP_ERROR_CHECK(i2c_master_read_byte(cmd, data+1, 0));
	ESP_ERROR_CHECK(i2c_master_read_byte(cmd, data+2, 0));
	ESP_ERROR_CHECK(i2c_master_read_byte(cmd, data+3, 0));
	ESP_ERROR_CHECK(i2c_master_read_byte(cmd, data+4, 0));
	ESP_ERROR_CHECK(i2c_master_read_byte(cmd, data+5, 0));
	ESP_ERROR_CHECK(i2c_master_read_byte(cmd, data+6, 0));
	ESP_ERROR_CHECK(i2c_master_read_byte(cmd, data+7, 0));
	ESP_ERROR_CHECK(i2c_master_read_byte(cmd, data+8, 0));
	ESP_ERROR_CHECK(i2c_master_read_byte(cmd, data+9, 0));
	ESP_ERROR_CHECK(i2c_master_read_byte(cmd, data+10, 0));
	ESP_ERROR_CHECK(i2c_master_read_byte(cmd, data+11, 0));
	ESP_ERROR_CHECK(i2c_master_read_byte(cmd, data+12, 0));
	ESP_ERROR_CHECK(i2c_master_read_byte(cmd, data+13, 0));
	ESP_ERROR_CHECK(i2c_master_read_byte(cmd, data+14, 0));
	ESP_ERROR_CHECK(i2c_master_read_byte(cmd, data+15, 0));
	ESP_ERROR_CHECK(i2c_master_read_byte(cmd, data+16, 0));
	ESP_ERROR_CHECK(i2c_master_read_byte(cmd, data+17, 0));
	ESP_ERROR_CHECK(i2c_master_read_byte(cmd, data+18, 0));
	ESP_ERROR_CHECK(i2c_master_read_byte(cmd, data+19, 0));
	ESP_ERROR_CHECK(i2c_master_read_byte(cmd, data+20, 0));
	ESP_ERROR_CHECK(i2c_master_read_byte(cmd, data+21, 0));
	ESP_ERROR_CHECK(i2c_master_read_byte(cmd, data+22, 0));
	ESP_ERROR_CHECK(i2c_master_read_byte(cmd, data+23, 1));

	ESP_ERROR_CHECK(i2c_master_stop(cmd));
	ESP_ERROR_CHECK(i2c_master_cmd_begin(I2C_NUM_0, cmd, NULL));
	i2c_cmd_link_delete(cmd);

	accel_x = (data[0] << 8) | data[1];
	accel_y = (data[2] << 8) | data[3];
	accel_z = (data[4] << 8) | data[5];
	gyro_x = (data[8] << 8) | data[9];
	gyro_y = (data[10] << 8) | data[11];
	gyro_z = (data[12] << 8) | data[13];
	mag_x = (data[16] << 8) | data[17];
	mag_y = (data[18] << 8) | data[19];
	mag_z = (data[20] << 8) | data[21];


	/* create data to send */
	data_imu->accel_x = accel_x;
	data_imu->accel_y = accel_y;
	data_imu->accel_z = accel_z;
	data_imu->gyro_x = gyro_x;
	data_imu->gyro_y = gyro_y;
	data_imu->gyro_z = gyro_z;

}
