#include <stdio.h>
#include "esp_types.h"
#include "soc/timer_group_struct.h"
#include "driver/periph_ctrl.h"
#include "driver/timer.h"
#include "driver/gpio.h"

#define TIMER_DIVIDER 80               /*!< Hardware timer clock divider, 80 to get 1MHz clock to timer */

void timer_init_start(void){
	int timer_group = TIMER_GROUP_0;
	int timer_idx = TIMER_0;
	timer_config_t config;
	config.counter_dir = TIMER_COUNT_UP;
	config.divider = TIMER_DIVIDER;
	config.intr_type = TIMER_INTR_LEVEL;
	config.counter_en = TIMER_PAUSE;
	timer_init(timer_group, timer_idx, &config);
	timer_pause(timer_group, timer_idx);
	timer_set_counter_value(timer_group, timer_idx, 0x00000000ULL);
	timer_start(timer_group, timer_idx);
}

double timer_get_value_s(void){
	double time_s;
	esp_err_t ret;

	ret = timer_get_counter_time_sec(TIMER_GROUP_0, TIMER_0, &time_s);
	assert(ret == ESP_OK);

	return time_s;
}

uint64_t timer_get_counter(void){
	uint64_t value;
	esp_err_t ret;

	ret = timer_get_counter_value(TIMER_GROUP_0, TIMER_0, &value);
	assert(ret == ESP_OK);

	return value;
}
