#ifndef WIFI_LIB_H
#define WIFI_LIB_H

#define EXAMPLE_WIFI_SSID CONFIG_WIFI_SSID
#define EXAMPLE_WIFI_PASS CONFIG_WIFI_PASSWORD

#include "esp_wifi.h"
#include "freertos/event_groups.h"

const static int CONNECTED_BIT = BIT0;
EventGroupHandle_t wifi_event_group;

void wifi_conn_init(void);

#endif
