#ifndef LEDS_H
#define LEDS_H

#include "sdkconfig.h"
#include "globals.h"

void leds_init(void);
void led_turn_on(int led);
void led_turn_off(int led);
#endif
