#include "leds.h"

#include <stdio.h>
#include "driver/gpio.h"


void led_turn_on(int led)
{
	gpio_set_level(led, 1);
}

void led_turn_off(int led)
{
	gpio_set_level(led, 0);
}

void leds_init(void)
{
	gpio_pad_select_gpio(LED_BLUE);
	gpio_pad_select_gpio(LED_GREEN);
	gpio_pad_select_gpio(LED_YELLOW);
	gpio_pad_select_gpio(LED_WHITE);

	gpio_set_direction(LED_BLUE, GPIO_MODE_OUTPUT);
	gpio_set_direction(LED_GREEN, GPIO_MODE_OUTPUT);
	gpio_set_direction(LED_YELLOW, GPIO_MODE_OUTPUT);
	gpio_set_direction(LED_WHITE, GPIO_MODE_OUTPUT);

	led_turn_off(LED_BLUE);
	led_turn_off(LED_GREEN);
	led_turn_off(LED_YELLOW);
	led_turn_off(LED_WHITE);
}
