#include "coap-handler.h"

#include "esp_log.h"
#include <netdb.h>
#include <globals.h>

const static char *TAG = "CoAP_client_LIB";
xQueueHandle xQueueCoapContexts;
extern TickType_t xTicksToWait;

uint16_t coap_next_message_id(void)
{
	static uint16_t current_message_id = 0;
	return current_message_id++;
}

coap_context_t * coap_get_free_context(void)
{
	coap_context_t * ctx = NULL;
	BaseType_t xStatus;

	xStatus = xQueueReceive( xQueueCoapContexts, &ctx, xTicksToWait );
	assert(xStatus == pdPASS);

	return ctx;
}

void coap_release_context(coap_context_t * ctx)
{
	xQueueSendToFront(xQueueCoapContexts, &ctx, xTicksToWait);
}

void coap_init(void)
{
	coap_address_t src_addr;
	coap_address_init(&src_addr);
	src_addr.addr.sin.sin_family = AF_INET;
	src_addr.addr.sin.sin_port = htons(COAP_DEFAULT_PORT);
	src_addr.addr.sin.sin_addr.s_addr = INADDR_ANY;

	coap_context_t * contexts[5];
	xQueueCoapContexts = xQueueCreate(5, sizeof(coap_context_t *));
	xQueueCoapAsync = xQueueCreate(5, sizeof(coap_async_state_t *));

	for (int i=0; i<5; i++) {
		contexts[i] = coap_new_context(&src_addr);
		coap_release_context(contexts[i]);
	}
}

void create_dst_addr(coap_address_t * dst_addr, const char * server_uri)
{
	coap_uri_t uri;
	char* phostname = NULL;
	struct ip4_addr *ip4_addr;
	struct hostent *hp;

	if (coap_split_uri((const uint8_t *)server_uri, strlen(server_uri), &uri) == -1) {
		ESP_LOGE(TAG, "CoAP server uri error");
		assert(false);
	}

	phostname = (char *)calloc(1, uri.host.length + 1);

	if (phostname == NULL) {
		ESP_LOGE(TAG, "calloc failed");
		assert(false);
	}

	memcpy(phostname, uri.host.s, uri.host.length);
	hp = gethostbyname(phostname);
	free(phostname);

	if (hp == NULL) {
		ESP_LOGE(TAG, "DNS lookup failed");
		vTaskDelay(1000 / portTICK_PERIOD_MS);
		assert(false);
	}

	ip4_addr = (struct ip4_addr *)hp->h_addr;
	//ESP_LOGI(TAG, "DNS lookup succeeded. IP=%s", inet_ntoa(*ip4_addr)); // Non re-entrant

	coap_address_init(dst_addr);
	dst_addr->addr.sin.sin_family      = AF_INET;
	dst_addr->addr.sin.sin_port        = htons(COAP_DEFAULT_PORT);
	dst_addr->addr.sin.sin_addr.s_addr = ip4_addr->addr;
}


void send_json_message( const cJSON * fld , const char * target_uri)
{
	coap_pdu_t* request = NULL;
	coap_address_t dst_addr;
	request = coap_new_pdu();
	coap_uri_t uri;
	unsigned char buf[3];

	coap_split_uri((const uint8_t *)target_uri, strlen(target_uri), &uri);
	create_dst_addr(&dst_addr, target_uri);
	const char * cdata = cJSON_Print(fld);

	coap_context_t * ctx;
	ctx = coap_get_free_context();

	if (request) {
		request->hdr->type = COAP_MESSAGE_NON;
		request->hdr->id   = coap_next_message_id();
		request->hdr->code = COAP_REQUEST_POST;

		coap_add_option(request, COAP_OPTION_URI_PATH, uri.path.length, uri.path.s);
		coap_add_option(request, COAP_OPTION_CONTENT_TYPE,
			coap_encode_var_bytes(buf, COAP_MEDIATYPE_APPLICATION_JSON), buf);
		coap_add_data(request, strlen(cdata), (unsigned char *) cdata);

		//coap_register_response_handler(ctx, message_handler);
		coap_send(ctx, ctx->endpoint, &dst_addr, request);

		coap_delete_pdu(request);
	}

	coap_release_context(ctx);
	free((char*)cdata);
}

