#ifndef PWMOUT_H
#define PWMOUT_H

#define GPIO_PWM0A_OUT CONFIG_PWM_A_OUT
#define GPIO_PWM1A_OUT CONFIG_PWM_B_OUT
#define GPIO_PWM0B_OUT CONFIG_PWM_C_OUT
#define GPIO_PWM1B_OUT CONFIG_PWM_D_OUT

void pwm_init(void);
void pwm_set_duty(int, float);

#endif
