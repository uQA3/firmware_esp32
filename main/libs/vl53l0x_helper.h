#ifndef VL53L03_H
#define VL53L03_H
#include "vl53l0x_def.h"
#include "vl53l0x_platform.h"

#define ADDR_VL53L0X CONFIG_ADDR_VL53L0X

VL53L0X_Error vl53l0x_init(VL53L0X_Dev_t *);

VL53L0X_Error vl53l0x_take_reading(VL53L0X_Dev_t *, VL53L0X_RangingMeasurementData_t *);

VL53L0X_Error vl53l0x_read_sensor(VL53L0X_Dev_t *, VL53L0X_RangingMeasurementData_t *);

#endif
