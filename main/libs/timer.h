#ifndef TIMER_LIB_H
#define TIMER_LIB_H

#define timer_get_elapsed(A, B) ((A) - (B))

void timer_init_start(void);
double timer_get_value_s(void);
uint64_t timer_get_counter(void);

#endif
