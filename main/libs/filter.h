#ifndef FILTER_H
#define FILTER_H

typedef struct{
	float angle;
	float q_bias;
	float angle_err;
	float q_angle;
	float q_gyro;
	float r_angle;
	float dt;
	char c_0;
	float pct_0;
	float pct_1;
	float e;
	float k_0;
	float k_1;
	float t_0;
	float t_1;
	float pdot[4];
	float pp[2][2];
} Quat;

void kfilter(float, float, Quat *);
void init_quat(Quat *);
float compute_motor(float);

#endif
