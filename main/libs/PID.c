#include "PID.h"

#include <math.h>
#include <stdio.h>

void reset_I(pid_info_t * _pid_info);

float get_pid(float error, float gyro, pid_info_t * _pid_info, pid_calibration_t * values, float delta_time)
{
	float output = 0;

	if (delta_time > 1) {
		delta_time = 0;

		// if this PID hasn't been used for a full second then zero
		// the intergator term. This prevents I buildup from a
		// previous fight mode from causing a massive return before
		// the integrator gets a chance to correct itself
		reset_I(_pid_info);
	}

	// Compute proportional component
	_pid_info->P = error * values->kp;
	output += _pid_info->P;

/*
	// Compute derivative component if time has elapsed
	if ((fabsf(values->kd) > 0) && (delta_time > 0)) {
		float derivative;

		if (isnan(_pid_info->_last_derivative)) {
			// we've just done a reset, suppress the first derivative
			// term as we don't want a sudden change in input to cause
			// a large D output change			
			derivative = 0;
			_pid_info->_last_derivative = 0;
		} else {
			derivative = (error - _pid_info->_last_error) / delta_time;
		}

	// discrete low pass filter, cuts out the
	// high frequency noise that can drive the controller crazy
	float RC = 1/(2*M_PI*values->fCut);
	derivative = _pid_info->_last_derivative + ((delta_time / (RC + delta_time)) *
		(derivative - _pid_info->_last_derivative));

	// update state
	_pid_info->_last_error = error;
	_pid_info->_last_derivative = derivative;

	// add in derivative component
	_pid_info->D = values->kd * derivative;
	output += _pid_info->D;
	}
*/

	// Compute derivative component if time has elapsed
	if ((fabsf(values->kd) > 0) && (delta_time > 0)) {
		float derivative;

		derivative = gyro;

		// add in derivative component
		_pid_info->D = values->kd * derivative;
		output += _pid_info->D;
	}

	// scale the P and D components
	//output *= scaler;
	//_pid_info->D *= scaler;
	//_pid_info->P *= scaler;

	// Compute integral component if time has elapsed
	if ((fabsf(values->ki) > 0) && (delta_time > 0)) {
		_pid_info->_integrator += (error * values->ki) * /*scaler * */ delta_time;
		if (_pid_info->_integrator < -values->imax)
			_pid_info->_integrator = -values->imax;
		else if (_pid_info->_integrator > values->imax)
			_pid_info->_integrator = values->imax;
		_pid_info->I = _pid_info->_integrator;
		output += _pid_info->_integrator;
	}

	//_pid_info->desired = output;
	return output;
}

void reset_I(pid_info_t * _pid_info)
{
	_pid_info->_integrator = 0;
	// we use NAN (Not A Number) to indicate that the last 
	// derivative value is not valid
	_pid_info->_last_derivative = NAN;
	_pid_info->I = 0;
}
