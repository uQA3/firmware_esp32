#ifndef PID_H
#define PID_H

#include <stdint.h>
typedef struct {
	float P;
	float D;
	float I;
	float _last_derivative;
	float _last_error;
	float _integrator;
} pid_info_t;

typedef struct {
	float kp;
	float kd;
	float ki;
	float fCut;
	float imax;
} pid_calibration_t;


float get_pid(float error, float gyro, pid_info_t * _pid_info, pid_calibration_t * values, float delta_time);

#endif
