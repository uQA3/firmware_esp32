#include "filter.h"
#include <math.h>

void kfilter(float accel, float gyro, Quat *quat)
{
	gyro = 0;
	quat->angle += (gyro - quat->q_bias) * quat->dt;
	quat->angle_err = accel - quat->angle;

	quat->pdot[0] = quat->q_angle - quat->pp[0][1] - quat->pp[1][0];
	quat->pdot[1] = -quat->pp[1][1];
	quat->pdot[2] = -quat->pp[1][1];
	quat->pdot[3] = quat->q_gyro;
	quat->pp[0][0] += quat->pdot[0] * quat->dt;
	quat->pp[0][1] += quat->pdot[1] * quat->dt;
	quat->pp[1][0] += quat->pdot[2] * quat->dt;
	quat->pp[1][1] += quat->pdot[3] * quat->dt;

	quat->pct_0 = quat->c_0 * quat->pp[0][0];
	quat->pct_1 = quat->c_0 * quat->pp[1][0];

	quat->e = quat->r_angle + quat->c_0 * quat->pct_0;

	quat->k_0 = quat->pct_0 / quat->e;
	quat->k_1 = quat->pct_1 / quat->e;

	quat->t_0 = quat->pct_0;
	quat->t_1 = quat->c_0 * quat->pp[0][1];

	quat->pp[0][0] -= quat->k_0 * quat->t_0;
	quat->pp[0][1] -= quat->k_0 * quat->t_1;
	quat->pp[1][0] -= quat->k_1 * quat->t_0;
	quat->pp[1][1] -= quat->k_1 * quat->t_1;

	quat->angle += quat->k_0 * quat->angle_err;
	quat->q_bias += quat->k_1 * quat->angle_err;
}

void init_quat(Quat *quat)
{
	quat->angle = 0;
	quat->q_bias = 0;
	quat->angle_err = 0;
	quat->q_angle = 0.1;
	quat->q_gyro = 0.1;
	quat->r_angle = 0.5;
	quat->dt = 0.1;
	quat->c_0 = 1;
	quat->pct_0 = 0;
	quat->pct_1 = 0;
	quat->e = 0;
	quat->k_0 = 0;
	quat->k_1 = 0;
	quat->t_0 = 0;
	quat->t_1 = 0;
	quat->pdot[0] = 0;
	quat->pdot[1] = 0;
	quat->pdot[2] = 0;
	quat->pdot[3] = 0;
	quat->pp[0][0] = 0;
	quat->pp[0][1] = 0;
	quat->pp[1][0] = 0;
	quat->pp[1][1] = 0;
}

float compute_motor(float motor)
{
	if (motor < 0)
		return 0;
	if (motor > 100)
		return 100;

	return motor;
}
