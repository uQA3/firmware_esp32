#ifndef GLOBALS_H
#define GLOBALS_H

#include <freertos/FreeRTOS.h>
#include <freertos/task.h>
#include <freertos/queue.h>
#include "cJSON.h"

#include <libs/PID.h>

#define LED_BLUE        CONFIG_LED_BLUE
#define LED_YELLOW      CONFIG_LED_YELLOW
#define LED_GREEN       CONFIG_LED_GREEN
#define LED_WHITE       CONFIG_LED_WHITE

#define TELEMETRY_URI  "coap://192.168.43.223/other"

#define MDNS_HOSTNAME "uqa3"
#define MDNS_INSTANCE "uQA3 Quadrotor"

typedef struct{
	cJSON * data;
	const char * uri;
} coapSendBuffer_t;

typedef enum{
	TYPE_IMU_TELEMETRY = 0,
	TYPE_HEARTBEAT,
	TYPE_MOTORS_TELEMETRY,
	TYPE_PID_TELEMETRY,
	TYPE_KALMAN_TELEMETRY,
} coapMessageType_t;

typedef struct{
	char recvbuf[48];
} udpData_t;

typedef struct{
	float motor_1;
	float motor_2;
	float motor_3;
	float motor_4;
} pwmData_t;

typedef struct{
	int accel_x;
	int accel_y;
	int accel_z;
	int gyro_x;
	int gyro_y;
	int gyro_z;
} imuData_t;

typedef struct
{
	float x;
	float y;
	float z;
} sensor3D_t;

xQueueHandle xQueuePWM;
xQueueHandle xQueueVL;
xQueueHandle xQueueBME;

xQueueHandle xQueueEventHandler;
xQueueHandle xQueueCoapSend;
xQueueHandle xQueueCoapAsync;

typedef enum{
	IMU_UPDATE = 0,
	UDP_UPDATE,
	PID_UPDATE,
} event_update_type_t;

struct _update_event_t {
	event_update_type_t type;
	union {
		struct {
			udpData_t data;
		} udp;
		
		struct {
			imuData_t data;
		} imu;
		struct {
			pid_calibration_t data;
		} pid;
	} data;
};
typedef struct _update_event_t update_event_t;

#endif
