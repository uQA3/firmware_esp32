/* uqa3 main core

*/
#include <stdio.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "driver/gpio.h"
#include "sdkconfig.h"
#include "globals.h"
#include "cJSON.h"
#include "nvs_flash.h"
#include "mdns.h"

#include <libs/udp_multicast.h>
#include <libs/wifi.h>
#include <services/baro/service.h>

#include <services/motors/service.h>
#include <services/imu/service.h>
#include <services/mitm/service.h>
#include <services/light_code/service.h>
#include <services/tof/service.h>
#include <services/coap/service.h>
#include <services/sys/service.h>

#include <services/service.h>

#define PIN_SDA CONFIG_PIN_SDA
#define PIN_CLK CONFIG_PIN_CLK
#define I2C_MASTER_ACK 0
#define I2C_MASTER_NACK 1

static void i2c_master_init()
{
	i2c_port_t i2c_master_port = I2C_NUM_0;
	i2c_config_t conf;
	conf.mode = I2C_MODE_MASTER;
	conf.sda_io_num = PIN_SDA;
	conf.sda_pullup_en = GPIO_PULLUP_ENABLE;
	conf.scl_io_num = PIN_CLK;
	conf.scl_pullup_en = GPIO_PULLUP_ENABLE;
	conf.master.clk_speed = 400000;
	
	ESP_ERROR_CHECK(i2c_param_config(i2c_master_port, &conf));
	ESP_ERROR_CHECK(i2c_driver_install(i2c_master_port, conf.mode, 0, 0, 0));
}

static config_service_t *services_list[] = {
	&service_mitm,
	&service_lights,
	&service_imu,
	&service_motors,
	&service_coap,
	&service_sys,
	NULL
};

void app_main()
{
	xQueueCoapSend = xQueueCreate(5, sizeof(coapSendBuffer_t));
	xQueueEventHandler = xQueueCreate(5, sizeof(update_event_t));

	i2c_master_init();
	ESP_ERROR_CHECK(nvs_flash_init());

	ESP_ERROR_CHECK( mdns_init() );
	ESP_ERROR_CHECK( mdns_hostname_set(MDNS_HOSTNAME) );
	ESP_ERROR_CHECK( mdns_instance_name_set(MDNS_INSTANCE) );

	wifi_conn_init();

	start_services(services_list);

	vTaskDelay(2000 / portTICK_PERIOD_MS);

	running();
}
