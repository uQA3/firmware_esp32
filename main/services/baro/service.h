#ifndef BARO_H
#define BARO_H

#include <services/service.h>

void task_bme280(void *ignore);

config_service_t service_baro;

#endif
