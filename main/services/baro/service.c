#include "service.h"

#include <libs/bme280.h>

void task_bme280(void *ignore)
{
	struct bme280_t bme280;
	bme280.bus_write = BME280_I2C_bus_write;
	bme280.bus_read = BME280_I2C_bus_read;
	bme280.dev_addr = BME280_I2C_ADDRESS2;
	bme280.delay_msec = BME280_delay_msek;

	s32 com_rslt;
	s32 v_uncomp_pressure_s32;
	s32 v_uncomp_temperature_s32;
	s32 v_uncomp_humidity_s32;
	u32 v_comp_pressure_s32;
	s32 v_comp_temperature_s32;
	u32 v_comp_humidity_s32;

	com_rslt = bme280_init(&bme280);

	com_rslt += bme280_set_oversamp_pressure(BME280_OVERSAMP_16X);
	com_rslt += bme280_set_oversamp_temperature(BME280_OVERSAMP_2X);
	com_rslt += bme280_set_oversamp_humidity(BME280_OVERSAMP_1X);

	com_rslt += bme280_set_standby_durn(BME280_STANDBY_TIME_1_MS);
	com_rslt += bme280_set_filter(BME280_FILTER_COEFF_16);

	com_rslt += bme280_set_power_mode(BME280_NORMAL_MODE);

	if (com_rslt == SUCCESS) {
		while(true) {
			vTaskDelay(1000 / portTICK_PERIOD_MS);
			/*Read Compensated data*/
			com_rslt = bme280_read_pressure_temperature_humidity(
				&v_comp_pressure_s32,
				&v_comp_temperature_s32,
				&v_comp_humidity_s32
				);
			if (com_rslt == SUCCESS) {
				printf("Press: %i Pa\n", v_comp_pressure_s32);
				printf("Temp: %i C\n", v_comp_temperature_s32);
				printf("Hum: %i %%\n", v_comp_humidity_s32);
			}
			/*Read Uncompensated data individually*/
			com_rslt = bme280_read_uncomp_pressure(&v_uncomp_pressure_s32);
			if (com_rslt == SUCCESS){
				printf("Press uncomp: %i\n", v_uncomp_pressure_s32);
			} else {
				printf("BME280 measure error. code: %d", com_rslt);
			}
			com_rslt = bme280_read_uncomp_temperature(&v_uncomp_temperature_s32);
			if (com_rslt == SUCCESS) {
			printf("Temp sin comp: %i\n", v_uncomp_temperature_s32);
			} else {
				printf("BME280 measure error. code: %d", com_rslt);
			}
			com_rslt = bme280_read_uncomp_humidity(&v_uncomp_humidity_s32);
			if (com_rslt == SUCCESS) {
				printf("Humedad sin comp: %i\n", v_uncomp_humidity_s32);
			} else {
				printf("BME280 measure error. code: %d", com_rslt);
			}
			/*Read Uncompensated data*/
			com_rslt = bme280_read_uncomp_pressure_temperature_humidity(
				&v_uncomp_pressure_s32,
				&v_uncomp_temperature_s32,
				&v_uncomp_humidity_s32);
			if (com_rslt == SUCCESS) {
				printf("Presion: %i Pa\n", v_uncomp_pressure_s32);
				printf("Temperatura: %i C\n", v_uncomp_temperature_s32);
				printf("Humedad: %i %%\n", v_uncomp_humidity_s32);
			} else {
				printf("BME280 measure error. code: %d", com_rslt);
			}
		}
	} else {
		printf("BME280 measure error. code: %d", com_rslt);
	}
	vTaskDelete(NULL);
}

config_service_t service_baro = {
	.name = "Baro Service",
	.entrypoint = task_bme280,
	.priority = MAXPRIO,
	.stacksize = 2048,
};
