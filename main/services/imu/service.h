#ifndef IMU_H
#define IMU_H
#include <globals.h>
#include <services/service.h>

#define ACCEL_SCALE_2G  16383.75f
#define ACCEL_SCALE_4G  8191.875f
#define GYRO_SCALE_250  131.07f
#define GYRO_SCALE_2000 16.38375f
#define ACCEL_X_OFFSET  200
#define ACCEL_Y_OFFSET  450
#define ACCEL_Z_OFFSET  -461
#define GYRO_X_OFFSET   -45
#define GYRO_Y_OFFSET   3
#define GYRO_Z_OFFSET   0
#define D2R             0.017452f

void task_mpu6050(void *);

config_service_t service_imu;

#endif
