#include "service.h"
#include <freertos/FreeRTOS.h>
#include <freertos/queue.h>
#include <freertos/task.h>

#include <services/service.h>
#include <libs/mpu6050.h>
#include <libs/coap-handler.h>

const TickType_t xTicksToWait = pdMS_TO_TICKS(20);

void send_telemetry( imuData_t * data)
{
	cJSON *root = NULL;
	cJSON *fld = NULL;
	BaseType_t xStatus;

	root = cJSON_CreateObject();
	cJSON_AddNumberToObject(root, "type", TYPE_IMU_TELEMETRY);
	cJSON_AddStringToObject(root, "uav", MDNS_HOSTNAME);
	cJSON_AddItemToObject(root, "data", fld = cJSON_CreateObject());
	cJSON_AddNumberToObject(fld, "accel_x", data->accel_x);
	cJSON_AddNumberToObject(fld, "accel_y", data->accel_y);
	cJSON_AddNumberToObject(fld, "accel_z", data->accel_z);
	cJSON_AddNumberToObject(fld, "gyro_x", data->gyro_x);
	cJSON_AddNumberToObject(fld, "gyro_y", data->gyro_y);
	cJSON_AddNumberToObject(fld, "gyro_z", data->gyro_z);

	coapSendBuffer_t message;
	message.uri = TELEMETRY_URI;
	message.data = root;

	xStatus = xQueueSendToFront(xQueueCoapSend, &message, xTicksToWait);
	if (xStatus != pdPASS)
		cJSON_Delete(root);
}

void task_mpu6050(void *ignore)
{
	int counter = 0, last_send_counter = 0;
	mpu6050_init();

	while (1) {
		imuData_t data;
		mpu6050_fetchData(&data);

		data.accel_x = (float) (data.accel_x - ACCEL_X_OFFSET) / ACCEL_SCALE_4G;
		data.accel_y = (float) (data.accel_y - ACCEL_Y_OFFSET) / ACCEL_SCALE_4G;
		data.accel_z = (float) (data.accel_z - ACCEL_Z_OFFSET) / ACCEL_SCALE_4G;
		data.gyro_x = ((float) (data.gyro_x - GYRO_X_OFFSET) / GYRO_SCALE_2000) * D2R;
		data.gyro_y = ((float) (data.gyro_y - GYRO_Y_OFFSET) / GYRO_SCALE_2000) * D2R;
		data.gyro_z = ((float) (data.gyro_z - GYRO_Z_OFFSET) / GYRO_SCALE_2000) * D2R;

		counter = xTaskGetTickCount();
		if ((float)(counter - last_send_counter)/portTICK_PERIOD_MS > 1) {
			send_telemetry(&data);
			last_send_counter = counter;
		}

		update_event_t imu_event;
		imu_event.type = IMU_UPDATE;
		imu_event.data.imu.data = data;

		xQueueSendToFront(xQueueEventHandler, &imu_event, xTicksToWait);
	}
	vTaskDelete(NULL);
}

config_service_t service_imu = {
	.name = "IMU Service",
	.entrypoint = task_mpu6050,
	.priority = 2,
	.stacksize = 2048,
};
