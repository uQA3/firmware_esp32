#ifndef SERVICE_H
#define SERVICE_H

#define MAXPRIO configMAX_PRIORITIES

typedef struct _service_t {
    char *const name;
    int stacksize;
    void * entrypoint;
    int priority;
} config_service_t;


void start_services(config_service_t *const *const services);

#endif

