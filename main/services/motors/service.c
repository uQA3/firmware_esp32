#include "service.h"

#include <libs/pwm.h>

void send_telemetry_motors( pwmData_t * data){
    cJSON *root = NULL;
    cJSON *fld = NULL;
    BaseType_t xStatus;

    root = cJSON_CreateObject();
    cJSON_AddNumberToObject(root, "type", TYPE_MOTORS_TELEMETRY);
    cJSON_AddStringToObject(root, "uav", MDNS_HOSTNAME);
    cJSON_AddItemToObject(root, "data", fld = cJSON_CreateObject());
    cJSON_AddNumberToObject(fld, "1", data->motor_1);
    cJSON_AddNumberToObject(fld, "2", data->motor_2);
    cJSON_AddNumberToObject(fld, "3", data->motor_3);
    cJSON_AddNumberToObject(fld, "4", data->motor_4);
    
    coapSendBuffer_t message;
    message.uri = TELEMETRY_URI;
    message.data = root;

    xStatus = xQueueSendToFront(xQueueCoapSend, &message, NULL);
    if (xStatus != pdPASS){
        cJSON_Delete(root);
    }
}

void task_motors(void *arg)
{
    pwm_init();

    xQueuePWM = xQueueCreate(1, sizeof(pwmData_t));
    BaseType_t xStatusPWM;
    pwmData_t data_pwm;
    const TickType_t xTicksToWait = pdMS_TO_TICKS(20);
    int counter = 0, last_send_counter = 0;

    for(;;) 
    {
        xStatusPWM = xQueueReceive( xQueuePWM, &data_pwm, xTicksToWait );
        if(xStatusPWM == pdPASS){
            pwm_set_duty(3, data_pwm.motor_3);
            pwm_set_duty(1, data_pwm.motor_1);
            pwm_set_duty(4, data_pwm.motor_4);
            pwm_set_duty(2, data_pwm.motor_2);

            counter = xTaskGetTickCount();
            if((float)(counter - last_send_counter)/portTICK_PERIOD_MS > 1){
                send_telemetry_motors(&data_pwm);
                last_send_counter = counter;
            }
        }
    }
    vTaskDelete( NULL );
}

config_service_t service_motors = {
    .name = "Motors Service",
    .entrypoint = task_motors,
    .priority = 3,
    .stacksize = 4096,
};
