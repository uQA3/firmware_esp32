#ifndef MOTORS_H
#define MOTORS_H

#include <globals.h>
#include <services/service.h>

void task_motors(void *arg);

config_service_t service_motors;

#endif
