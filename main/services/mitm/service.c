#include "service.h"
#include <cJSON.h>
#include <globals.h>
#include <math.h>

#include <libs/filter.h>
#include <libs/kalman.h>
#include <libs/timer.h>
#include <libs/PID.h>
#include <libs/leds.h>


void send_telemetry_pid(pid_calibration_t * data)
{
	cJSON *root = NULL;
	cJSON *fld = NULL;
	BaseType_t xStatus;

	root = cJSON_CreateObject();
	cJSON_AddNumberToObject(root, "type", TYPE_PID_TELEMETRY);
	cJSON_AddStringToObject(root, "uav", MDNS_HOSTNAME);
	cJSON_AddItemToObject(root, "data", fld = cJSON_CreateObject());
	cJSON_AddNumberToObject(fld, "kp", data->kp);
	cJSON_AddNumberToObject(fld, "ki", data->ki);
	cJSON_AddNumberToObject(fld, "kd", data->kd);
	cJSON_AddNumberToObject(fld, "fCut", data->fCut);
	cJSON_AddNumberToObject(fld, "imax", data->imax);

	coapSendBuffer_t message;
	message.uri = TELEMETRY_URI;
	message.data = root;

	xStatus = xQueueSendToFront(xQueueCoapSend, &message, NULL);
	if (xStatus != pdPASS)
		cJSON_Delete(root);
}

void send_telemetry_kalman(float roll, float pitch, float yaw, float initial_yaw, float dt)
{
	cJSON *root = NULL;
	cJSON *fld = NULL;
	BaseType_t xStatus;

	root = cJSON_CreateObject();
	cJSON_AddNumberToObject(root, "type", TYPE_KALMAN_TELEMETRY);
	cJSON_AddStringToObject(root, "uav", MDNS_HOSTNAME);
	cJSON_AddItemToObject(root, "data", fld = cJSON_CreateObject());
	cJSON_AddNumberToObject(fld, "roll", roll);
	cJSON_AddNumberToObject(fld, "pitch", pitch);
	cJSON_AddNumberToObject(fld, "yaw", yaw);
	cJSON_AddNumberToObject(fld, "i_yaw", initial_yaw);
	cJSON_AddNumberToObject(fld, "dt", dt);

	coapSendBuffer_t message;
	message.uri = TELEMETRY_URI;
	message.data = root;

	xStatus = xQueueSendToFront(xQueueCoapSend, &message, NULL);
	if (xStatus != pdPASS)
		cJSON_Delete(root);
}

void task_mitm(void *pvParameter)
{
	BaseType_t xStatus;

	const TickType_t xTicksToWait = pdMS_TO_TICKS(20);

	sensor3D_t accel;
	sensor3D_t gyro;
	sensor3D_t magneto;

	float ipitch = 0, iroll = 0, iyaw = 0, ithrottle = -1000;
	float pitch = 0, roll = 0, yaw = 0;
	float pitch_out = 0, roll_out = 0, yaw_out = 0;

	timer_init_start();
	float dt;
	float time_current;
	float time_previous = timer_get_value_s();

	int counter = 0, last_send_counter = 0;

	kalman_filter_t kf;
	kalman_filter_params_t kf_params = {
		0.2, 0.2, 0.2, 0.2, /* P */
		0.0003, 0.0003, 0.0003, 0.0003, /* Q */
		0.3, 0.3, 0.3, /* Ra */
		0.007, 0.007, 0.007}; /* Rm */

	kalman_filter_alloc (&kf);
	kalman_filter_init (&kf, &kf_params);

	pid_info_t pid_roll_state;
	pid_info_t pid_pitch_state;
	pid_info_t pid_yaw_state;
	pid_calibration_t pid_values = {
		.kp = 9,
		.kd = 1.9,
		.ki = 0,
		.fCut = 1000,
		.imax = 100,
	};

	static float initial_yaw = 0;

	udpData_t udp_data;
	cJSON *root;
	imuData_t imu_data;

	update_event_t ev;
	while (1) {
		xStatus = xQueueReceive( xQueueEventHandler, &ev, xTicksToWait );

		if (xStatus != pdPASS)
			continue;

		switch (ev.type) {
		case UDP_UPDATE:
			udp_data = ev.data.udp.data;

			root = cJSON_Parse(udp_data.recvbuf);
			ithrottle = cJSON_GetObjectItem(root,"T")->valueint;
			ipitch = cJSON_GetObjectItem(root,"P")->valueint;
			iroll = cJSON_GetObjectItem(root,"R")->valueint;
			iyaw = cJSON_GetObjectItem(root,"Y")->valueint;

			iroll = (float) iroll / 180 * M_PI;
			ipitch = (float) ipitch / 180 * M_PI;

			break;
		case IMU_UPDATE:
			imu_data = ev.data.imu.data;

			accel.y = imu_data.accel_x;
			accel.x = -1.0 * imu_data.accel_y;
			accel.z = -1.0 * imu_data.accel_z;
			gyro.x = imu_data.gyro_x;
			gyro.y = imu_data.gyro_y;
			gyro.z = imu_data.gyro_z;
			break;
		case PID_UPDATE:
			pid_values = ev.data.pid.data;
			send_telemetry_pid(&pid_values);
			break;
		default:
			return;
		}

		led_turn_on(LED_YELLOW);

		const TickType_t xTicksToWait = pdMS_TO_TICKS(20);

		time_current = timer_get_value_s();
		dt = time_current - time_previous;
		time_previous = time_current;

		kalman_filter_predict_stage (&kf, &gyro, dt);
		kalman_filter_accel_update_stage (&kf, &accel) ;
		/*kalman_filter_mag_update_stage (&kf, &magneto) ;*/

		roll = kalman_filter_get_roll (&kf);
		pitch = kalman_filter_get_pitch (&kf);
		yaw = kalman_filter_get_yaw (&kf);

		pitch += 0.022;
		roll -= 0.006;

		if (initial_yaw == 0)
			initial_yaw = yaw;


		roll_out = get_pid((iroll-roll), -gyro.y, &pid_roll_state, &pid_values, dt);
		pitch_out = get_pid((ipitch-pitch), gyro.x, &pid_pitch_state, &pid_values, dt);
		yaw_out = get_pid((initial_yaw-yaw), -gyro.z, &pid_yaw_state, &pid_values, dt);

		/* create data to send */
		pwmData_t data_pwm = {0};
		data_pwm.motor_1 = compute_motor(ithrottle - pitch_out - roll_out - yaw_out);
		data_pwm.motor_2 = compute_motor(ithrottle - pitch_out + roll_out + yaw_out);
		data_pwm.motor_3 = compute_motor(ithrottle + pitch_out + roll_out - yaw_out);
		data_pwm.motor_4 = compute_motor(ithrottle + pitch_out - roll_out + yaw_out);

		counter = xTaskGetTickCount();
		if ((float)(counter - last_send_counter)/portTICK_PERIOD_MS > 1) {
			send_telemetry_kalman(roll, pitch, yaw, initial_yaw, dt);
			last_send_counter = counter;
		}

		xQueueSendToFront( xQueuePWM, &data_pwm, xTicksToWait );

		led_turn_off(LED_YELLOW);
	}

	kalman_filter_free (&kf);

	vTaskDelete( NULL );
}

config_service_t service_mitm = {
	.name = "MITM Service",
	.entrypoint = task_mitm,
	.priority = 2,
	.stacksize = 4096,
};
