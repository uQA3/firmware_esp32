#ifndef LIGHT_CODE_H
#define LIGHT_CODE_H

#include <services/service.h>

void start_task(void *);
void running(void);

config_service_t service_lights;

#endif
