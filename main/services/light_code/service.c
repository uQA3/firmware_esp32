#include "service.h"

#include <stdio.h>
#include <driver/gpio.h>
#include <sdkconfig.h>
#include <globals.h>

#include <libs/leds.h>

void start_task(void *pvParameter)
{
	leds_init();

	for (int i=0; i<2; i++) {
		vTaskDelay(200 / portTICK_PERIOD_MS);
		led_turn_off(LED_BLUE);
		led_turn_on(LED_GREEN);
		vTaskDelay(200 / portTICK_PERIOD_MS);
		led_turn_off(LED_GREEN);
		led_turn_on(LED_YELLOW);
		vTaskDelay(200 / portTICK_PERIOD_MS);
		led_turn_off(LED_YELLOW);
		led_turn_on(LED_WHITE);
		vTaskDelay(200 / portTICK_PERIOD_MS);
		led_turn_off(LED_WHITE);
		led_turn_on(LED_BLUE);
	}
	led_turn_off(LED_BLUE);
	vTaskDelete(NULL);
}

void running(void)
{
	led_turn_off(LED_YELLOW);
	led_turn_off(LED_GREEN);
	led_turn_off(LED_WHITE);
	led_turn_on(LED_BLUE);
}

config_service_t service_lights = {
	.name = "Lights Service",
	.entrypoint = start_task,
	.priority = 1,
	.stacksize = 2048,
};
