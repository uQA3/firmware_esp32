#include "service.h"

#include <freertos/FreeRTOS.h>
#include <freertos/task.h>

void start_services(config_service_t *const *const services){
    for(int i=0; services[i] != NULL; i++){
        xTaskCreate(
            services[i]->entrypoint,
            services[i]->name,
            services[i]->stacksize,
            NULL,
            services[i]->priority,
            NULL);
    };
};
