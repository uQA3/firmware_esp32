#include "endpoints.h"

#include <cJSON.h>
#include <libs/PID.h>

extern TickType_t xTicksToWait;

static void joystick_handler(coap_context_t *ctx, struct coap_resource_t *resource,
	const coap_endpoint_t *local_interface, coap_address_t *peer,
	coap_pdu_t *request, str *token, coap_pdu_t *response)
{
	udpData_t data;
	coap_async_state_t * async = NULL;

	strncpy(data.recvbuf, (const char *) request->data, 48);

	update_event_t udp_event;
	udp_event.type = UDP_UPDATE;
	udp_event.data.udp.data = data;

	xQueueSendToFront( xQueueEventHandler, &udp_event, xTicksToWait );

	async = coap_register_async(ctx, peer, request,
		COAP_ASYNC_SEPARATE | COAP_ASYNC_CONFIRM, (void*)"ack");
	if(async != NULL)
		xQueueSendToFront(xQueueCoapAsync, &async, xTicksToWait);
}

static void pid_handler(coap_context_t *ctx, struct coap_resource_t *resource, 
	const coap_endpoint_t *local_interface, coap_address_t *peer,
	coap_pdu_t *request, str *token, coap_pdu_t *response)
{
	pid_calibration_t data;
	coap_async_state_t * async = NULL;

	cJSON *root = cJSON_Parse((const char *)request->data);
	data.kp = (float) cJSON_GetObjectItem(root,"kp")->valuedouble;
	data.kd = (float) cJSON_GetObjectItem(root,"kd")->valuedouble;
	data.ki = (float) cJSON_GetObjectItem(root,"ki")->valuedouble;
	data.fCut = (float) cJSON_GetObjectItem(root,"fCut")->valuedouble;
	data.imax = (float) cJSON_GetObjectItem(root,"imax")->valuedouble;

	update_event_t pid_event;
	pid_event.type = PID_UPDATE;
	pid_event.data.pid.data = data;

	xQueueSendToFront( xQueueEventHandler, &pid_event, xTicksToWait );

	async = coap_register_async(ctx, peer, request,
		COAP_ASYNC_SEPARATE | COAP_ASYNC_CONFIRM, (void*)"ack");
	if(async != NULL)
		xQueueSendToFront(xQueueCoapAsync, &async, xTicksToWait);
}

void register_coap_endpoints(coap_context_t * ctx)
{
	coap_resource_t* resource = NULL;

	resource = coap_resource_init((unsigned char *)"joystick", 8, 0);
	coap_register_handler(resource, COAP_REQUEST_POST, joystick_handler);
	coap_add_resource(ctx, resource);

	resource = coap_resource_init((unsigned char *)"pid", 3, 1);
	coap_register_handler(resource, COAP_REQUEST_POST, pid_handler);
	coap_add_resource(ctx, resource);
}
