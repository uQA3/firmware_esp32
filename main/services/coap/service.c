#include "service.h"

#include <sys/socket.h>
#include <netdb.h>
#include "esp_log.h"
#include "esp_event_loop.h"

#include <libs/wifi.h>
#include <libs/coap-handler.h>

#include "endpoints.h"

#define COAP_DEFAULT_TIME_SEC 5
#define COAP_DEFAULT_TIME_USEC 0

extern TickType_t xTicksToWait;

const static char *TAG = "CoAP";
coap_context_t *ctx = NULL;

void coap_client_sender(void * unused)
{
	coapSendBuffer_t buffer;
	BaseType_t xStatus;

	while (1) {
		xStatus = xQueueReceive(xQueueCoapSend, &buffer, xTicksToWait);
		if (xStatus != pdPASS)
			continue;

		send_json_message(buffer.data, buffer.uri);
		cJSON_Delete(buffer.data);
	}

	vTaskDelete(NULL);
}

static void send_async_response(coap_context_t *ctx, const coap_endpoint_t *local_if,
	coap_async_state_t * async)
{
	coap_pdu_t *response;
	unsigned char buf[3];
	const char* response_data = async->appdata;

	response = coap_pdu_init(async->flags & COAP_MESSAGE_CON,
		COAP_RESPONSE_CODE(205), 0, COAP_MAX_PDU_SIZE);
	response->hdr->id = coap_next_message_id();
	if (async->tokenlen) {
		coap_add_token(response, async->tokenlen, async->token);
	}
	coap_add_option(response, COAP_OPTION_CONTENT_TYPE, coap_encode_var_bytes(buf, COAP_MEDIATYPE_TEXT_PLAIN), buf);
	coap_add_data(response, strlen(response_data), (unsigned char *)response_data);
	if (coap_send(ctx, local_if, &async->peer, response) == COAP_INVALID_TID) {
	}
	coap_delete_pdu(response);
	coap_async_state_t *tmp;
	coap_remove_async(ctx, async->id, &tmp);
	coap_free_async(async);
	async = NULL;
}

void coap_server_receiver(void * ble){
	BaseType_t xStatus;
	coap_async_state_t * async = NULL;
	while (1) {
		xStatus = xQueueReceive(xQueueCoapAsync, &async, xTicksToWait);
		if (xStatus != pdPASS)
			continue;
		send_async_response(ctx, ctx->endpoint, async);
	}
	vTaskDelete(NULL);
}

void coap_task(void *p)
{
	fd_set readfds;
	struct timeval tv;
	int flags, result;

	while (1) {
		xEventGroupWaitBits(wifi_event_group, CONNECTED_BIT,
			false, true, portMAX_DELAY);
		ESP_LOGI(TAG, "Connected to AP");

		coap_init();

		xTaskCreate(&coap_client_sender, "client_sender_task", 4096, NULL, 1, NULL);
		xTaskCreate(&coap_server_receiver, "server_receiver_task", 4096, NULL, 3, NULL);

		ctx = coap_get_free_context();
		if (ctx) {
			flags = fcntl(ctx->sockfd, F_GETFL, 0);
			fcntl(ctx->sockfd, F_SETFL, flags|O_NONBLOCK);

			tv.tv_usec = COAP_DEFAULT_TIME_USEC;
			tv.tv_sec = COAP_DEFAULT_TIME_SEC;

			register_coap_endpoints(ctx);

			while (1) {
				FD_ZERO(&readfds);
				FD_CLR( ctx->sockfd, &readfds );
				FD_SET( ctx->sockfd, &readfds );
				result = select( ctx->sockfd+1, &readfds, 0, 0, &tv );
				if (result > 0) {
					if (FD_ISSET(ctx->sockfd, &readfds))
						coap_read(ctx);
				} else if (result < 0) {
					break;
				} else {
					ESP_LOGE(TAG, "select timeout");
				}
			}
			coap_release_context(ctx);
		}
	}
	vTaskDelete(NULL);
}

config_service_t service_coap = {
	.name = "COAP Service",
	.entrypoint = coap_task,
	.priority = 2,
	.stacksize = 4096,
};
