#ifndef ENDPOINTS_H
#define ENDPOINTS_H

#include <globals.h>
#include "coap.h"

void register_coap_endpoints(coap_context_t * ctx);

#endif
