#ifndef COAP_CLIENT_H
#define COAP_CLIENT_H

#include <globals.h>
#include <services/service.h>
#include "coap.h"
#include <cJSON.h>

void coap_task(void *);

config_service_t service_coap;

#endif
