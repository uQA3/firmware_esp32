#include "service.h"
#include <libs/coap-handler.h>

const TickType_t xTicksToWaitBle = pdMS_TO_TICKS(20);

void send_heartbeat(void){
    cJSON *root = NULL;
    cJSON *fld = NULL;
    double uptime = xTaskGetTickCount() * portTICK_PERIOD_MS / 1000;

    root = cJSON_CreateObject();
    cJSON_AddNumberToObject(root, "type", TYPE_HEARTBEAT);
    cJSON_AddStringToObject(root, "uav", MDNS_HOSTNAME);
    cJSON_AddItemToObject(root, "data", fld = cJSON_CreateObject());
    cJSON_AddNumberToObject(fld, "uptime_s", uptime);

    coapSendBuffer_t message;
    message.uri = TELEMETRY_URI;
    message.data = root;

    xQueueSendToFront(xQueueCoapSend, &message, xTicksToWaitBle);
}

void task_sys(void *ignore) {

    while(1) {
        send_heartbeat();

        vTaskDelay(1000/portTICK_PERIOD_MS);
    }
    vTaskDelete(NULL);
}

config_service_t service_sys = {
    .name = "SYS Service",
    .entrypoint = task_sys,
    .priority = 0,
    .stacksize = 2048,
};
