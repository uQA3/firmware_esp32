#ifndef SYS_SERVICE_H
#define SYS_SERVICE_H
#include <globals.h>
#include <services/service.h>

void task_sys(void *);

config_service_t service_sys;

#endif
