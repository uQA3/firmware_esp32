#ifndef TOF_H
#define TOF_H
#include <services/service.h>
#include <libs/vl53l0x_helper.h>

void task_vl53l0x(void);

config_service_t service_tof;

#endif
