#include "service.h"
#include <driver/i2c.h>

void task_vl53l0x()
{

    static VL53L0X_Dev_t vl53l0x_dev;
    VL53L0X_RangingMeasurementData_t measurement_data;

    vl53l0x_dev.i2c_port_num = I2C_NUM_0;
    vl53l0x_dev.i2c_address = ADDR_VL53L0X;

    VL53L0X_Error status = vl53l0x_init(&vl53l0x_dev);

    if (status != VL53L0X_ERROR_NONE){
         printf("ERROR VL53L03");
         assert(false);
    }

    while(1)
    {
        vl53l0x_read_sensor(&vl53l0x_dev, &measurement_data);
        printf("Measured distance: %i \n", measurement_data.RangeMilliMeter);
        vTaskDelay(250 / portTICK_PERIOD_MS);
    }

}

config_service_t service_tof = {
    .name = "TOF Service",
    .entrypoint = task_vl53l0x,
    .priority = MAXPRIO,
    .stacksize = 2048,
};
