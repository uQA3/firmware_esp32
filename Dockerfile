FROM debian:unstable

# RUN echo 'Acquire::HTTP::Proxy "http://192.168.1.11:3142";' >> /etc/apt/apt.conf.d/01proxy \ 
#     && echo 'Acquire::HTTPS::Proxy "false";' >> /etc/apt/apt.conf.d/01proxy;

RUN apt-get update && apt-get install -y --no-install-recommends \
    git \
    wget \
    eatmydata

# Python2 dependencies for esp-idf
RUN apt-get update && eatmydata apt-get install -y --no-install-recommends \
    python \
    python-pip \
    python-setuptools \
    python-wheel

WORKDIR /root/esp

# Install espressif
RUN wget https://dl.espressif.com/dl/xtensa-esp32-elf-linux64-1.22.0-80-g6c4433a-5.2.0.tar.gz && \
    tar -xzf xtensa-esp32-elf-linux64-1.22.0-80-g6c4433a-5.2.0.tar.gz && \
    rm xtensa-esp32-elf-linux64-1.22.0-80-g6c4433a-5.2.0.tar.gz

# install esp-idf on known version. We might want to update this later.
RUN eatmydata git clone --recursive https://github.com/espressif/esp-idf.git && \
    cd esp-idf && git checkout 3276a1316f66a923ee2e75b9bd5c7f1006d160f5 && \
    eatmydata python -m pip install -r requirements.txt
    # eatmydata python3 -m pip install -r esp-idf/requirements.txt && \

WORKDIR /root

RUN apt-get update && eatmydata apt-get install -y --no-install-recommends \
    gcc \
    make \
    cmake \
    libncurses-dev \
    flex \
    bison \
    gperf

ENV PATH "${PATH}:/root/esp/xtensa-esp32-elf/bin"
ENV IDF_PATH /root/esp/esp-idf

# Developing tools
RUN apt-get update && eatmydata apt-get install -y --no-install-recommends \
    vim \
    ctags \
    tmux

# Add vl53l0x component
RUN git clone https://github.com/kylehendricks/esp32-vl53l0x $IDF_PATH/components/esp32-vl53l0x && \
    cd $IDF_PATH/components/esp32-vl53l0x && \
    git checkout 03e92d2950d86f103dd4a8b4e655296181004468
COPY vl53l0x-cmake $IDF_PATH/components/esp32-vl53l0x/CMakeLists.txt

# Add uqa3 component for Kconfig stuff only
COPY esp32-uqa3 $IDF_PATH/components/esp32-uqa3

RUN cd /root/esp/esp-idf && git submodule update --init --recursive

WORKDIR /root/workdir
